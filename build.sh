#!/bin/sh
cwd=$(pwd)
echo "$cwd"
#compile the mongoose library
makemongoose() {
  cd src/lib/mongoose
  cmake -DEXAMPLES=OFF
  make
}

makecurlcpp() {
  cd "$cwd"
  cd src/lib/curlcpp/build
  cmake ..
  make
}
# -DCMAKE_CXX_COMPILER=g++-4.8 -DCMAKE_CC_COMPILER=gcc-4.8
makeDB() {
  cd db
  echo "Setting up SQLite3 database"
  echo ".read setup.sql" | sqlite3 thronedb.db
  [ -e thronedb.db ] &&  echo "Done setting up the database" || echo "Error setting up the database"
}

cleanDB(){
  echo "Cleaning database"
  rm -rf src/db/thronedb.db
}

makeThrone() {
  echo "$cwd"
  cd "$cwd"
  cd src
  make
  mv throne ../throne
}

makeThroneTests() {
  echo "$cwd"
  cd "$cwd"
  cd src
  make thronetests
  mv thronetests ../thronetests
}

cleanThrone() {
  cleanDB
  echo "Performing clean"
  cd src
  echo "cleaning .o files..."
  make clean
  echo "done"
  cd "$cwd"
}

cleanMongoose() {
  cd src/lib/mongoose
  echo "cleaning mongoose..."
  rm -rf cmake_install.cmake
  rm -rf CMakeCache.txt
  rm -rf Makefile
  rm -rf CMakeFiles
  rm -rf *.a
  rm -rf MongooseConfig.cmake
}

cleancurl() {
  cd "$cwd"
  cd src/lib/curlcpp/build
  echo "cleaning curl"
  rm -rf cmake_install.cmake
  rm -rf CMakeCache.txt
  rm -rf Makefile
  rm -rf CMakeFiles
  rm -rf curlcppConfig.cmake
  rm -rf ../build/src
}

if [ "$1" = "-c" ]
then
  cleanThrone
  if [ "$2" = "-f" ]
  then
    cleanMongoose
    cleancurl
  fi
  echo "Done cleaning"
elif [ "$1" = "-r" ]
then
  cleanThrone
  if [ "$2" = "-f" ]
  then
    cleanMongoose
    cleancurl
  fi
  cd "$cwd"
  makemongoose
  makecurlcpp
  makeThrone
  makeDB
elif [ "$1" = "-tests" ]
then
  makemongoose
  makecurlcpp
  makeThroneTests
  makeDB
else
  makemongoose
  makecurlcpp
  makeThrone
  makeDB
fi

# Fetch existing key
# RESULT=$(curl -s $URL/bar/baz)
# test "$RESULT" = "success" || exit 1

exit 0
