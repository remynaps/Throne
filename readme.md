# ![pageres](doc/logo/logo.png)

# Throne

[![build status](https://gitlab.com/remynaps/Throne/badges/master/build.svg)](https://gitlab.com/remynaps/Throne/commits/master)

DISCLAIMER!
This repository will (most likely) be abandoned by the end of june, as this is a project mainly intended for educational purposes.

Throne is an MQTT based home automation application that can communicate with a number of protocolls.
It uses a RESTFUL API to ensure compatibility with modern applications, and uses a lightweight MQTT client to communicate with other MQTT clients.
Support for macros is currently in BETA.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisities

Throne has a number of dependencies. Most of these are included in the project and do not need any setup.

#### On OSX

1. install mosquitto
```
brew install mosquitto
```
2. install SQLite3
```
brew install sqlite3
```

#### On Ubuntu

1. install mosquitto
```
apt-get install mosquitto
apt-get install libmosquitto-dev
```
2. install SQLite3
```
apt-get install sqlite3
```
3. install Curl
```
apt-get install curl
```

### Installing

A step by step series of examples that tell you how to get a development env running.
Throne compiles with either Clang or GCC.

First. Clone the repository into your preferred folder.
```
git clone https://gitlab.com/remynaps/Throne.git
```

2. Build the project

   This will build the project and compile it into the throne executable. which will be placed in the same directory as the build script.

```
sh build.sh
```

3. Run Throne

```
./throne
```

## Running the tests

If throne is already build, you need to clean it first.

```
sh build.sh -c
```

Next, run the build script again and provide the -tests flag. This will build Throne in its testing mode and will provide a seperate executable.
```
sh build.sh -tests
```

run the tests
```
./thronetests
```

## Built With

* VS Code
* OSX/Linux
* Atom
* VIM

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Remy Span** - *Initial work* - [remynaps](https://github.com/remynaps)
* **Hidde Westerhof** - *Initial work* - [Dr.Bl0nde](https://gitlab.com/u/Nigel2x4)
* **Pieter Van Der Berg** - *Initial work* - [Draaky](https://gitlab.com/u/Draaky)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
