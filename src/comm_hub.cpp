#include "header/comm_hub.hpp"
#include "header/sender_locator.hpp"
#include "lib/mongoose/mongoose.h"
#include <string>
#include <iostream>
#include "header/logger.hpp"
#include <stdlib.h>
#include <unistd.h>

Comm_Hub::Comm_Hub()
{
  init();
  infinite_loop_count = 0;
  last_topic = "";
}

Comm_Hub::~Comm_Hub()
{
  std::cout<< "stopping components.." << std::endl;
  //delete all elements in the action map
  delete mqtt_;
  delete db_layer;
}

void Comm_Hub::init()
{
   mqtt_ = new Mqtt_client("test-throne","dank/memes","192.168.0.200",1883, true);
   db_layer = new Db_Layer();
   //mqtt controllers
   Mqtt_controller *cont = new Mqtt_api(this);
   mqtt_->register_controller(cont);
}

void Comm_Hub::init_db()
{
  // DB_Manager::instance().QuerySelect("Actions");
}

//update coponenets if needed
void Comm_Hub::update()
{
}

void Comm_Hub::send_mqtt(const char *data, const char *topic)
{
    process_action(topic);
    //mqtt_->send_message(data,topic);
}

//process notification from a sender
void Comm_Hub::process_alert(const char *topic)
{
    //process_action(topic);
    //mqtt_->send_message(data,topic);
}

//process static sensor data
void Comm_Hub::process_sensor(const char *topic)
{
  std::cout << "sensor" << std::endl;
}

void Comm_Hub::create_action(string data,string destdata, string source, string dest, string type)
{
  std::shared_ptr<Action> action;
  if(!check_action_valid(source,dest))
  {
    LOG("ERROR","ACTION NOT VALID");
    return;
  }
  //locate the given type in the sender dictionary.
  //default to mqtt if not found
  if(SenderFactory::instance().Find(type))
  {
    action = std::shared_ptr<Action>(new Sender_Action(SenderFactory::instance().Get(type),data,destdata,source,dest));
  }
  else //default to mqtt
  {
    action =  std::shared_ptr<Action>(new Mqtt_Action(mqtt_,data,destdata,source,dest));
  }
  //action = null. terminate

  if ( actions.find(source) == actions.end() ) {
      cout << "no action found. creating.." << endl;
      //InsertTopic(source);
      std::vector<std::shared_ptr<Action>> local_actions;
      local_actions.push_back(action);
      actions.insert(std::make_pair(source,local_actions));
    } else {
      //action source exsists
      actions[source].push_back(action);
    }
    db_layer->InsertAction(action);
}

bool Comm_Hub::check_action_valid(string source, string dest)
{
    if(source != "" && dest != "")
    {
      return true;
    }
    LOG("ACTION","action was incomplete. source or dest was empty");
    return false;
}

//return a list of actions assosiated with the device
std::vector<std::shared_ptr<Action>> Comm_Hub::get_actions(string source)
{
    std::cout << source << std::endl;
    std::unordered_map<std::string,vector< std::shared_ptr<Action>>>::const_iterator got = actions.find(source);
    if ( got == actions.end() )
      std::cout << "not found" << std::endl;
    else
    {
      return got->second;
    }
    //vector is not nullable so return an empty one
    return std::vector<std::shared_ptr<Action>>();
}

//return a list of actions assosiated with the device
std::vector< std::shared_ptr<Action>> Comm_Hub::get_all_actions()
{
  std::vector< std::shared_ptr<Action>> results;
  for ( auto it = actions.begin(); it != actions.end(); ++it )
  {
    std::vector< std::shared_ptr<Action>> local_results = it->second;
    for ( std::shared_ptr<Action> action : local_results)
    {
      results.push_back(action);
    }
  }
  return results;
}

//return a list of actions assosiated with the device
std::vector<std::string> Comm_Hub::get_all_action_sources()
{
  std::vector<std::string> results;
  for ( auto it = actions.begin(); it != actions.end(); ++it )
  {
    results.push_back(it->first);
  }
  return results;
}

//execute every action in the vector if it is not empty
void Comm_Hub::process_action(string msg, string data)
{
    if(msg == last_topic)
    {
      cout << "same as last" << endl;
      infinite_loop_count++;
      if(infinite_loop_count > 5)
      {
        LOG("INFINITE LOOP", "infinite loop detected: exiting...");
        LOG("TIP", "check if your destination for an Action is the same as a trigger for another action");
        sleep(1);
        infinite_loop_count = 0;
        return;
      }
    }
    //check_topic(msg);
    std::vector< std::shared_ptr<Action>> local_actions = get_actions(msg);
    last_topic = msg;
    if(!local_actions.empty())
    {
      for( std::shared_ptr<Action> & action : local_actions)
      {
        action->Execute(data);
      }
    }
    else{
      std::cout << "no actions found" << std::endl;
    }
}

void Comm_Hub::deleteHash(string topic){
  actions.erase(topic);
}
