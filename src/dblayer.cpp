#include "header/dblayer.hpp"

Db_Layer::Db_Layer()
{

}

Db_Layer::~Db_Layer()
{

}

bool Db_Layer::check_topic(string topic)
{
  for(std::vector<std::tuple<std::string, std::string>> &rows: DB_Manager::instance().executeSelectStatement("Topic")){
    for(std::tuple<std::string, std::string> &row : rows){
      if(std::get<1>(row) == topic){
        return false;
      }
    }
  }

  return true;
}

int Db_Layer::get_Topic_id(string topic)
{
  for(std::vector<std::tuple<std::string, std::string>> &rows: DB_Manager::instance().executeSelectStatement("Topic")){
    for(std::tuple<std::string, std::string> &row : rows){
      if(std::get<1>(row) == topic){
        return std::stoi(std::get<0>(row));
      }
    }
  }

  //
  // for (int i = 0; i < results.size(); i++) {
  //   if(results[i][1][1] == topic)
  //   {
  //     std::string result = results[i][0][1];
  //     return std::stoi(result);
  //   }
  // }
  return -1;
}

std::vector<std::vector<std::tuple<std::string, std::string>>> Db_Layer::Get_All(string name)
{
  return DB_Manager::instance().executeSelectStatement(name);
}

bool Db_Layer::create_topic(string topic)
{
  if(topic != "" && check_topic(topic))
  {
      DB_Manager::instance().executeInsertStatement("Topic", {"name"}, {{topic}});
      return true;
  }
  return false;
}

void Db_Layer::InsertAction( std::shared_ptr<Action> action)
{
  if(action != NULL)
  {
    DB_Manager::instance().executeInsertStatement("Action", {"fromDevice","fromTopic","fromData","toTopic","toData","compType"}, {{"1",action->source, action->data,action->dest,action->destData,"1"}});
  }
}
