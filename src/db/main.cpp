
#include <iostream>
#include "./sqlite3.h"
#include <vector>

const char* kDb_path = "./db/test.db";
sqlite3* db;
char *zErrMsg = 0;
int rc;

std::string selectBuilder(std::string table, std::string args[], int argSize){
  std::string selectSQL = "SELECT ";
  if(argSize == 0){
    for(int i = 0; i < argSize; i++){
      if (i == argSize -1){
          selectSQL += args[i];
      }
      else{
          selectSQL += (args[i] + ",");
      }
    }
  }
  else{
      selectSQL += "* ";
  }
  selectSQL + " FROM ";
  selectSQL + table;
  return selectSQL;
}

std::string getSelectSQL(std::string table, std::string args[], int argSize){
  return selectBuilder(table, args, argSize) + ";";
}

std::string getSelectSQL(std::string table, std::string args[], int argSize, std::string whereClause, std::string whereOperator, std::string whereValue){
  return selectBuilder(table, args, argSize) + " WHERE " + whereClause + " " + whereOperator + " ? ;";
}

std::string getInsertSQL(std::string table, std::string args[], int argSize){
    std::string insertSQL = "INSERT INTO " + table + "(";
    for(int i = 0; i < argSize; i++){
      if (i == argSize -1){
          insertSQL += args[i];
      }
      else{
        insertSQL += (args[i] + ",");
      }
    }
    insertSQL += ") VALUES (";
    for(int i = 0; i < argSize; i++){
      if ( i == argSize -1){
        insertSQL += "?";
      }
      else{
        insertSQL += "? ,";
      }
    }
    insertSQL += ");";
    return insertSQL;
}

int main(){

    rc = sqlite3_open(kDb_path, &db);

    std::string table = "table1";
    std::string args[] = {"name"};
    std::string vals[] = {"Elon Musk", "Dick Bruin"};

    sqlite3_stmt *insertstmt = NULL;
    for(int i = 0; i < sizeof(vals) / sizeof( vals[0]); i++){
        int argsize = sizeof(args) / sizeof( args[0]);
        sqlite3_prepare_v2(db, getInsertSQL(table, args, argsize).c_str(), -1, &insertstmt, NULL);
        sqlite3_bind_text(insertstmt, 1, vals[i].c_str(), sizeof(vals[i]), NULL);
        sqlite3_step(insertstmt);
    }

    sqlite3_stmt *selectstmt = NULL;
    for(int i = 0; i < sizeof(vals) / sizeof( vals[0]); i++){
        int argsize = sizeof(args) / sizeof( args[0]);
        sqlite3_prepare_v2(db, getInsertSQL(table, args, argsize).c_str(), -1, &selectstmt, NULL);
        sqlite3_bind_text(selectstmt, 1, vals[i].c_str(), sizeof(vals[i]), NULL);
    }


    while(SQLITE_ROW == (rc = sqlite3_step(selectstmt))) {
		int col;
		printf("Found row\n");
		for(col=0; col<sqlite3_column_count(selectstmt); col++) {
			// Note that by using sqlite3_column_text, sqlite will coerce the value into a string
			printf("\tColumn %s(%i): '%s'\n",
				sqlite3_column_name(selectstmt, col), col,
				sqlite3_column_text(selectstmt, col));
  		}
  	}
    sqlite3_finalize(selectstmt);
    sqlite3_finalize(insertstmt);
    sqlite3_close(db);

}
