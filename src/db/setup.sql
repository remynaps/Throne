DROP TABLE Topic;
DROP TABLE Device;
DROP TABLE Type;
DROP TABLE Archive;
DROP TABLE Action;
DROP TABLE Subscriber;
DROP TABLE Day;
DROP VIEW NamedAction;

CREATE TABLE Topic(
  id INTEGER PRIMARY KEY,
  name varchar(255) NOT NULL
);

CREATE TABLE Device(
  id INTEGER PRIMARY KEY,
  parentId int,
  FOREIGN KEY(parentId) REFERENCES Device(id)
);

CREATE TABLE Type(
  id INTEGER PRIMARY KEY,
  name varchar(255) NOT NULL
);

CREATE TABLE Archive(
  id INTEGER PRIMARY KEY,
  value varchar(255) NOT NULL,
  logtime int NOT NULL
);

CREATE TABLE Action(
  id INTEGER PRIMARY KEY,
  fromDevice int NOT NULL,
  fromTopic int NOT NULL,
  fromData varchar(255) NOT NULL,
  toTopic int NOT NULL,
  toData varchar(255) NOT NULL,
  compType int,
  FOREIGN KEY(fromTopic) REFERENCES Topic(id),
  FOREIGN KEY(fromDevice) REFERENCES Device(id),
  FOREIGN KEY(toTopic) REFERENCES Topic(id),
  FOREIGN KEY(compType) REFERENCES TYPE(id)
);

CREATE TABLE Day(
  id INTEGER PRIMARY KEY,
  device int NOT NULL,
  value varchar(255) NOT NULL,
  logtime int NOT NULL,
  FOREIGN KEY(device) REFERENCES Device(id)
);

CREATE TABLE Subscriber(
  device int NOT NULL,
  topic int NOT NULL,
  FOREIGN KEY(device) REFERENCES Device(id),
  FOREIGN KEY(topic) REFERENCES Topic(id)
);

CREATE VIEW NamedAction AS
SELECT *
FROM  Action
LEFT JOIN Topic t1 ON t1.id = Action.toTopic
LEFT JOIN Topic t2 ON t2.id = Action.fromTopic;
