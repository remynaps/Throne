/*
Hidde Westerhof
This class handles the data comming in from agents.
*/
#ifndef CORE_HEADER_XML_PARSER_HPP_
#define CORE_HEADER_XML_PARSER_HPP_

#include "../lib/pugixml/pugixml.hpp"

#include <string>
#include <map>
#include <vector>
#include <iostream>

class XML_Data{
private:
  const char* key_;
  const char* data_;
public:
  XML_Data(const char* key, const char* data){key_ = key; data_ = data; }
  std::pair<const char*, const char*> getData(){ return std::make_pair(key_, data_); }
};

class XML_Parser{
public:
  XML_Parser(const char* filename = "./system.xml");
  ~XML_Parser() { filecontent_.clear(); };

  void readXML() { readXML(xmlDocRoot_.first_child()); }
  std::vector<XML_Data> getXMLcontent();

private:
  void readXML(pugi::xml_node value);

  pugi::xml_node xmlDocRoot_;
  pugi::xml_document xmlDocument_;

  std::vector<XML_Data> filecontent_;

  const char* kXMLRootNode = "system";
  const char* kXMLComponentNode = "component";

};

#endif
