
#ifndef MQTT_HPP_
#define MQTT_HPP_

#include <mosquittopp.h>
#include <iostream>
#include <map>
#include <vector>
#include "mqtt_controller.hpp"

using namespace std;

class Mqtt_client : public mosqpp::mosquittopp
{
private:
 const char     *     host;
 const char    *     id;
 const char    *     topic;
 int                port;
 int                keepalive;
 std::vector<Mqtt_controller*> controllers;

 void on_connect(int rc);
 void on_subcribe(int mid, int qos_count, const int *granted_qos);
 void on_disconnect(int rc);
 void on_publish(int mid);
public:
 Mqtt_client(const char *id, const char * _topic, const char *host, int port, bool async = false);
 Mqtt_client();
 ~Mqtt_client();
 void start(bool async);
 bool send_message(const char * _message, const char * _topic);
 void on_message(const struct mosquitto_message *message);
 void register_controller(Mqtt_controller *cont);
 std::vector<Mqtt_controller*> get_controllers();
};

#endif
