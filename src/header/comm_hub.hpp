/*
Hidde Westerhof
This object handles communication between the API and the server.
*/
#ifndef CORE_HEADER_WEB_CONTROLLER_HPP_
#define CORE_HEADER_WEB_CONTROLLER_HPP_

#include "api.hpp"
#include "mqtt_client.hpp"
#include "mqtt_api.hpp"
#include "action.hpp"
#include <unordered_map>
#include "../lib/mongoose/mongoose.h"
#include "dblayer.hpp"

class Comm_Hub{
public:
  Comm_Hub();
  ~Comm_Hub();
  Db_Layer *db_layer;
  void init();
  void update();
  void send_mqtt(const char* data, const char *topic);
  void create_action(string source,string destdata, string dest, string data, string type = "mqtt");
  void process_action(string msg, string data="");
  void process_alert(const char *topic);
  void process_sensor(const char *topic);
  void deleteHash(std::string topic);
  std::vector< std::shared_ptr<Action>> get_all_actions();
  std::vector<std::string> get_all_action_sources();
  Api *api_;
  std::vector< std::shared_ptr<Action>> get_actions(string source);
  bool check_topic(string msg);
private:
  void init_db();
   Mqtt_client *mqtt_;
   //define a hashmap containing actions for every device in the network
   std::unordered_map<std::string,std::vector<std::shared_ptr<Action>>> actions;
   bool check_action_valid(string source, string dest);
   int infinite_loop_count;
   std::string last_topic;
};

#endif
