#ifndef CORE_HEADER_MQTT_API_HPP_
#define CORE_HEADER_MQTT_API_HPP_

#include "mqtt_controller.hpp"
#include "structs.hpp"

class Comm_Hub;

class Mqtt_api : public Mqtt_controller
{
public:
  Mqtt_api(Comm_Hub *hub);
  Mqtt_api();
  ~Mqtt_api();
  void init();
  void start();
  Comm_Hub *hub;
private:
  //insert all method defs
  void handle_light(const struct mqtt_message* msg);
  void handle_sensor(const struct mqtt_message* msg);
  void handle_treshold(const struct mqtt_message* msg);
};


#endif
