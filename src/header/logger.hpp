/*
Hidde Westerhof
This object writes to a log file.
*/

/*
  Last worked on: 4 11
  Class creation
*/
#ifndef CORE_HEADER_LOGGER_HPP_
#define CORE_HEADER_LOGGER_HPP_
#define LOG(type, message)(Logger::log(message, type));

#include <sstream>
#include <fstream>
#include <time.h>
#include <vector>
#include <string>
#include <iostream>

class Logger{
public:
  static void log(std::string message, std::string type){
    write(format(type, message));
  };
private:
  std::vector<std::string> logStack;

  static std::string getCurrentTime(){
    time_t rawtime;
    struct tm * timeinfo;
    char buffer [80];

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime (buffer,80,"%F %T",timeinfo);
    return std::string(buffer);
  }
  static std::string format(std::string type, std::string msg){
    std::string timestamp = getCurrentTime();
    for(char &c : type) c = toupper(c);
    char* x = new char[type.length() + msg.length() + timestamp.length() + 32];
    sprintf(x, "[%s][%s] %s", timestamp.c_str(), type.c_str(), msg.c_str() );
    std::string retval = x;
    delete[] x;
    return retval;
  }
  static void write(std::string msg){
    std::ofstream logFile;
    logFile.open ("./crash.log", std::ios::app); //set flag for appending
    logFile << msg << "\r\n";
    logFile.close();
  }
};

#endif
