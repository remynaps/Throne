#ifndef I_SEND_HPP_
#define I_SEND_HPP_

#include <iostream>
#include <map>
#include "./structs.hpp"


class ISender
{
public:
 virtual ~ISender() {};
 virtual void send(const char* route) = 0;
 virtual void send_data(const char* route, const char* data) = 0;
 virtual void send_async(const char* url, const char* data = "") = 0;
};

#endif
