/*
Hidde Westerhof
This object defines the basic functions of an Application Program Interface (API).
Both the agent_api and client_api implement this interface.
*/
#ifndef CORE_HEADER_API_HPP_
#define CORE_HEADER_API_HPP_

#include "../lib/mongoose/mongoose/JsonController.h"

class Comm_Hub;

using namespace Mongoose;

class Api : public Mongoose::JsonController{
public:
  Api(Comm_Hub *hub);
  ~Api();
  void init();
  void start();
  Comm_Hub *hub;
private:
  void http_GET(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void http_koek(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void GET_Device_Status(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void GET_JSON_Device_Status(Mongoose::Request &request, Mongoose::JsonResponse &response);
  void POST_Action(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void Get_Actions(Mongoose::Request &request, Mongoose::JsonResponse &response);
  void Get_Topics(Mongoose::Request &request, Mongoose::JsonResponse &response);
  void Get_Subscribers(Mongoose::Request &request, Mongoose::JsonResponse &response);
  void Process_Action(Mongoose::Request &request, Mongoose::StreamResponse &response);
  bool Parse_Action_Json(Json::Value &root, Json::Value &source,Json::Value &destination,
    Json::Value &type,Json::Value &sourcedata,Json::Value &destinationdata);
  void POST_Topic(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void Call_action(Mongoose::Request &request, Mongoose::StreamResponse &response);
  void Call_Delete(Mongoose::Request &request, Mongoose::StreamResponse &response);
};



#endif
