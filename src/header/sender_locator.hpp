
#ifndef SEND_LOC_HPP_
#define SEND_LOC_HPP_

#include "sender.hpp"

//simple method to return a new instance of a given type
template<typename Type> ISender* createType() {
    return new Type;
}

//factory to register sender derived classes
class SenderFactory
{
private:
    SenderFactory() {};
    typedef ISender* (*ComponentFactoryFuncPtr)();
    typedef std::map<std::string, ComponentFactoryFuncPtr> map_type;

    map_type m_Map;

public:
  static SenderFactory& instance(){
    static SenderFactory instance;
    return instance;
  }
  ~SenderFactory()
  {
    m_Map.clear();
  }

  //template method to register a new type in the factory
  template<typename Type>
  void AddType(std::string componentName){
        ComponentFactoryFuncPtr function = &createType<Type>;
      m_Map.insert(std::make_pair(componentName, function));
  };

  //get a new instance from the map and return null if not found
  ISender* Get(std::string componentName){
     map_type::iterator iter = m_Map.find(componentName);
     if(iter != m_Map.end())
     {
       return iter->second();
     }
     return NULL;
  };

  bool Find(std::string componentName){
     map_type::iterator iter = m_Map.find(componentName);
     if(iter != m_Map.end())
     {
       return true;
     }
     return false;
  };
};

#endif
