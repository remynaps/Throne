#ifndef STRUCTS_
#define STRUCTS_
#include <string>
#include <iostream>

struct mqtt_message{
      int mid;
      char *topic;
      void *payload;
      int payloadlen;
      int qos;
      bool retain;
};

struct http_message{
      std::string topic;
      std::string payload;
};




#endif
