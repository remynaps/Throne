/*
Hidde Westerhof
db_manager is responsible for sending data to the database.
included for inclusion purposes.
*/
#ifndef CORE_HEADER_DBMANAGER_HPP_
#define CORE_HEADER_DBMANAGER_HPP_

#include <iostream>
#include "../lib/SQLite/sqlite3.h"
#include <tuple>
#include <vector>
#include <string>

class DB_Manager{
  public:
    static DB_Manager& instance(){
      static DB_Manager instance;
      return instance;
    }
    ~DB_Manager(){ }

    void executeInsertStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string>> _values);
    std::vector<std::vector<std::tuple<std::string, std::string>>> executeSelectStatement(std::string table, std::string value, std::string whereClause, std::string whereOperator, std::vector<std::string> args = {});
    std::vector<std::vector<std::tuple<std::string, std::string>>> executeSelectStatement(std::string table, int value, std::string whereClause, std::string whereOperator, std::vector<std::string> args = {});
    std::vector<std::vector<std::tuple<std::string, std::string>>> executeSelectStatement(std::string table, std::vector<std::string> args = {});
    void executeUpdateStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string>> _values, std::string whereClause, std::string whereOperator, std::string whereValue);
    void executeUpdateStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string>> _values, std::string whereClause, std::string whereOperator, int whereValue);
    void executeDeleteStatement(std::string table, std::string whereClause, std::string whereOperator, std::string whereValue);
    void executeDeleteStatement(std::string table, std::string whereClause, std::string whereOperator, int whereValue);

  private:
    DB_Manager(){}

    const char* kDb_path = "./src/db/thronedb.db";
    sqlite3* db;
    int rc;

    sqlite3_stmt *statement = NULL;

    //opening and closing db
    void openDB();
    void closeDB();

    //Statement preps
    void prepInsertStatement(std::vector<std::string> vals, std::string query);
    void prepSelectStatement(int id, std::string query); //selecting value is id
    void prepSelectStatement(std::string value, std::string query); //selecting value as string
    void prepSelectStatement(std::string query);
    void prepUpdateStatement(std::vector<std::string> vals, std::string whereValue, std::string query);
    void prepUpdateStatement(std::vector<std::string> vals, int whereValue, std::string query);
    void prepDeleteStatement(std::string whereValue, std::string query);
    void prepDeleteStatement(int whereValue, std::string query);
    //processing SELECT results
    std::vector<std::vector<std::tuple<std::string, std::string>>> fetchSelectResults();

    //CRUD query builder
    std::string getInsertSQL(std::string table, std::vector<std::string> args);
    std::string getSelectSQL(std::string table, std::vector<std::string> args);
    std::string getSelectSQL(std::string table, std::vector<std::string> args, std::string whereClause, std::string whereOperator);
    std::string getUpdateSQL(std::string table, std::vector<std::string> args, std::string whereClause, std::string whereOperator);
    std::string getDeleteSQL(std::string table, std::string whereClause, std::string whereOperator);

    //selectBuilder for general purpose of Select Query
    std::string selectBuilder(std::string table, std::vector<std::string> args);

};
#endif
