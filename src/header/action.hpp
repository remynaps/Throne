#ifndef CORE_HEADER_ACTION_HPP_
#define CORE_HEADER_ACTION_HPP_

#include "mqtt_client.hpp"
#include "htpp_sender.hpp"

class Action {
public:
  Action(std::string _message, std::string _topic,std::string data,std::string destData);
  //~Action();
  virtual bool Execute(string data="", bool async = true) =0;
  std::string source;
  std::string dest;
  std::string data;
  std::string destData;
protected:
};

// a simple macro like action that will send an MQTT message
class Mqtt_Action : public Action {
public:
  Mqtt_Action(Mqtt_client *mqtt_,std::string data,std::string destData,std::string source, std::string dest);
  //~Mqtt_Action();
  bool Execute(string data="", bool async = true) override;
  std::string dest_data;
private:
  Mqtt_client *mqtt_;
};

// a simple macro like action that will send an HTTP message
class Sender_Action : public Action {
public:
  Sender_Action(ISender *htpp,std::string data,std::string dest_data,std::string source, std::string dest);
  //~Mqtt_Action();
  bool Execute(string data="", bool async = true) override;
private:
  ISender *sender_;
};


//comparison actions.
//these actions can (independetly of the type of sender) compare and execute actions
//--------------------------------------------------------------------------

class ComparisonAction : public Action {
public:
  ComparisonAction(ISender *sender,std::string source, std::string dest,std::string data,std::string dest_data);
  using Action::Execute;
  virtual bool Execute(int data, bool async = true) = 0;
private:
  ISender *sender_;
};

class LargerThenAction : public ComparisonAction {
public:
  LargerThenAction(ISender *sender,int compare_data, int data,std::string source, std::string dest,std::string sdata,std::string dest_data);
  bool Execute(int data, bool async = true) override;
private:
  ISender *sender_;
  int _compare_data;
  int data;
};

class SmallerThenAction : public ComparisonAction {
public:
  SmallerThenAction(ISender *sender,int compare_data, int data,std::string source, std::string dest,std::string sdata,std::string dest_data);
  bool Execute(int data, bool async = true) override;
private:
  ISender *sender_;
  int _compare_data;
  int data;
};

class EqualsAction : public ComparisonAction {
public:
  EqualsAction(ISender *sender,int compare_data, int data,std::string source, std::string dest,std::string sdata,std::string dest_data);
  bool Execute(int data, bool async = true) override;
private:
  ISender *sender_;
  int _compare_data;
  int data;
};

//overload class to compare two strings
class StringComparisonAction : public Action {
public:
  StringComparisonAction(ISender *sender,std::string data,std::string compare_data,std::string source, std::string dest);
  bool Execute(string data="", bool async = true) override;
private:
  ISender *sender_;
  std::string _compare_data;
};

#endif
