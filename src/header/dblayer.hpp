
#ifndef CORE_HEADER_DBLAYER_HPP_
#define CORE_HEADER_DBLAYER_HPP_

#include "api.hpp"
#include "mqtt_client.hpp"
#include "mqtt_api.hpp"
#include "action.hpp"
#include <unordered_map>
#include "../lib/mongoose/mongoose.h"
#include "db_manager.hpp"

class Db_Layer{
public:
  Db_Layer();
  ~Db_Layer();
  std::vector< std::shared_ptr<Action>> get_all_actions();
  std::vector<std::string> get_all_action_sources();
  bool delete_action(string actionId);
  bool create_topic(string topic);
  std::vector< std::shared_ptr<Action>> get_actions(string source);
  int get_Topic_id(string topic);
  bool check_topic(string msg);
  std::vector<std::vector<std::tuple<std::string, std::string>>> Get_All(string name);
  void InsertTopic(string Topic);
  void InsertAction( std::shared_ptr<Action> action);
private:
  void init_db();
   //define a hashmap containing actions for every device in the network
};

#endif
