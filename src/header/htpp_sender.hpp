#ifndef CORE_HEADER_HTPPSENDER_HPP_
#define CORE_HEADER_HTPPSENDER_HPP_

#include <iostream>
#include <ostream>
#include <thread>
#include <fstream>
#include "sender.hpp"
#include "../lib/curlcpp/include/curl_easy.h"
#include "../lib/curlcpp/include/curl_multi.h"
#include "../lib/curlcpp/include/curl_exception.h"

class Http_sender : public ISender
{
public:
  ~Http_sender();
  void send(const char* route);
  void send_data(const char* route, const char* data = "");
  void send_async(const char* url, const char* data = "");
};

#endif
