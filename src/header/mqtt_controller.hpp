#ifndef MQTT_CON_HPP_
#define MQTT_CON_HPP_

#include <iostream>
#include <map>
#include "./structs.hpp"
using namespace std;

#define add_Route(url, controllerType, method) \
    register_Route(url, new Mqtt_handler<controllerType>(this, &controllerType::method ));


class Mqtt_controller;

//function pointer typedef
typedef void (Mqtt_controller::*pfunc)(const struct mqtt_message*);

class Mqtt_handler_base
{
    public:
        virtual void *process(const struct mqtt_message* msg)=0;
};

//template class for the method that handles a given route in the controller.
//inspiration from mongoose
template<typename T>
class Mqtt_handler : public Mqtt_handler_base
{
    public:
        typedef void (T::*fPtr)(const struct mqtt_message*);

        Mqtt_handler(T *controller_, fPtr function_): controller(controller_), function(function_)
        {
        }

        void *process(const struct mqtt_message* msg)
        {
            (controller->*function)(msg);
        }

    protected:
        T *controller;
        fPtr function;
};


class Mqtt_controller
{
protected:
  //define the routes using a map op the route itself and a pointer to a function
  map<string, Mqtt_handler_base*> routes;

public:
 Mqtt_controller();
 virtual void register_Route(std::string route, Mqtt_handler_base *handler);
 //~Mqtt_controller();

void process(const struct mqtt_message* msg);

//methods
void handle_light(std::string data);

};

#endif
