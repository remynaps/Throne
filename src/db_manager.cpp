
#include "./header/db_manager.hpp"

//get SQL strings
std::string DB_Manager::getInsertSQL(std::string table, std::vector<std::string> args){
    std::string insertSQL = "INSERT INTO " + table + "(";
    for(int i = 0; i < args.size(); i++){
      if (i == args.size() -1){
          insertSQL += args[i];
      }
      else{
        insertSQL += (args[i] + ",");
      }
    }
    insertSQL += ") VALUES (";
    for(int i = 0; i < args.size(); i++){
      if ( i == args.size() -1){
        insertSQL += "?";
      }
      else{
        insertSQL += "? ,";
      }
    }
    insertSQL += ");";
    return insertSQL;
}

std::string DB_Manager::selectBuilder(std::string table, std::vector<std::string> args){
  int argSize = args.size();
  std::string selectSQL = "SELECT ";
  if(argSize != 0){
    for(int i = 0; i < argSize; i++){
      if (i == argSize -1){
          selectSQL += args[i];
      }
      else{
          selectSQL += (args[i] + ",");
      }
    }
  }
  else{
      selectSQL += "* ";
  }
  selectSQL += " FROM ";
  selectSQL += table;
  return selectSQL;
}

std::string DB_Manager::getSelectSQL(std::string table, std::vector<std::string> args){
  return selectBuilder(table, args) + ";";
}

std::string DB_Manager::getSelectSQL(std::string table, std::vector<std::string> args, std::string whereClause, std::string whereOperator){
  return selectBuilder(table, args) + " WHERE " + whereClause + " " + whereOperator + " ? ;";
}

std::string DB_Manager::getUpdateSQL(std::string table, std::vector<std::string> args, std::string whereClause, std::string whereOperator){
  std::string updateSQL = "UPDATE " + table + " SET ";
  int argSize = args.size();
  for(int i = 0; i < argSize; i++){
    if(i == argSize -1){
        updateSQL += args[i] +"= ? ";
    }
    else{
      updateSQL += args[i] + "= ? ,";
    }
  }

  return updateSQL += "WHERE " + whereClause + " " + whereOperator + "?;";
}

std::string DB_Manager::getDeleteSQL(std::string table, std::string whereClause, std::string whereOperator){
  return "DELETE FROM " + table + " WHERE " + whereClause  + " " + whereOperator + " ? ;";
}

std::vector<std::vector<std::tuple<std::string, std::string>>> DB_Manager::fetchSelectResults(){
  std::vector<std::vector<std::tuple<std::string, std::string>>> results_;
      while(SQLITE_ROW == (rc = sqlite3_step(statement))) {
    		int col;
        std::vector<std::tuple<std::string, std::string>> row;
    		for(col=0; col<sqlite3_column_count(statement); col++) {
            row.push_back(std::make_tuple(sqlite3_column_name(statement, col), std::string((const char*)sqlite3_column_text(statement, col))));
      		}
        results_.push_back(row);
      }
      return results_;
}

//prepare statements
void DB_Manager::prepInsertStatement(std::vector<std::string> vals, std::string query){
  sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL);
  for(int i = 0; i < vals.size(); i++){
      if(SQLITE_OK != sqlite3_bind_text(statement, (1+i), vals[i].c_str(), vals[i].size(), NULL)) {
         fprintf(stderr, "Error binding value in Insert Query. RC:(%i). Error %s\n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
         sqlite3_close(db);
         exit(1);
      }
   }
   if(SQLITE_DONE != sqlite3_step(statement)) {
    fprintf(stderr, "Insert statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
  }
}

void DB_Manager::prepSelectStatement(int id, std::string query){
  if(SQLITE_OK != sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL)) {
		fprintf(stderr, "Can't prepare select statment %s (%i): %s\n", query.c_str(), rc, sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(1);
	}
  if(SQLITE_OK != sqlite3_bind_int64(statement, 1, id)) {
    fprintf(stderr, "Error binding value in Query (%i): %s \n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
    //sqlite3_close(db);
    //exit(1);
  }
}
void DB_Manager::prepSelectStatement(std::string value, std::string query){
  if(SQLITE_OK != sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL)) {
		fprintf(stderr, "Can't prepare select statment %s (%i): %s\n", query.c_str(), rc, sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(1);
	}
  if(SQLITE_OK != sqlite3_bind_text(statement, 1, value.c_str(), sizeof(value), NULL)) {
    fprintf(stderr, "Error binding value in Query (%i): %s \n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
    //sqlite3_close(db);
    //exit(1);
  }
}
void DB_Manager::prepSelectStatement(std::string query){
  sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL);
}

void DB_Manager::prepUpdateStatement(std::vector<std::string> vals, std::string whereValue, std::string query){
  sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL);
  for(int i = 0; i < vals.size(); i++){
      if(SQLITE_OK != sqlite3_bind_text(statement, (1+i), vals[i].c_str(), sizeof(vals[i]), NULL)) {
         fprintf(stderr, "Error binding value in Update Query. RC:(%i). Error %s\n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
         sqlite3_close(db);
         exit(1);
      }
   }
   if(SQLITE_OK != sqlite3_bind_text(statement, vals.size()+1, whereValue.c_str(), sizeof(whereValue), NULL)) {
      fprintf(stderr, "Error binding Where Value in Update Query. RC:(%i). Error %s\n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
      sqlite3_close(db);
      exit(1);
   }
   if(SQLITE_DONE != sqlite3_step(statement)) {
    fprintf(stderr, "Update statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
  }
}
void DB_Manager::prepUpdateStatement(std::vector<std::string> vals, int whereValue, std::string query){
  sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL);
  for(int i = 0; i < vals.size(); i++){
      if(SQLITE_OK != sqlite3_bind_text(statement, (1+i), vals[i].c_str(), sizeof(vals[i]), NULL)) {
         fprintf(stderr, "Error binding value in Update Query. RC:(%i). Error %s\n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
         sqlite3_close(db);
         exit(1);
      }
   }
   if(SQLITE_OK != sqlite3_bind_int64(statement, vals.size()+1, whereValue)) {
      fprintf(stderr, "Error binding Where Value in Update Query. RC:(%i). Error %s\n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
      sqlite3_close(db);
      exit(1);
   }
   if(SQLITE_DONE != sqlite3_step(statement)) {
    fprintf(stderr, "Update statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
  }
}

void DB_Manager::prepDeleteStatement(std::string whereValue, std::string query){
  if(SQLITE_OK != sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL)) {
		fprintf(stderr, "Can't prepare select statment %s (%i): %s\n", query.c_str(), rc, sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(1);
	}
  if(SQLITE_OK != sqlite3_bind_text(statement, 1, whereValue.c_str(), sizeof(whereValue), NULL)) {
    fprintf(stderr, "Error binding value in Query (%i): %s \n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
    //sqlite3_close(db);
    //exit(1);
  }
  if(SQLITE_DONE != sqlite3_step(statement)) {
   fprintf(stderr, "Update statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
 }
}
void DB_Manager::prepDeleteStatement(int whereValue, std::string query){
  if(SQLITE_OK != sqlite3_prepare_v2(db, query.c_str(), -1, &statement, NULL)) {
		fprintf(stderr, "Can't prepare select statment %s (%i): %s\n", query.c_str(), rc, sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(1);
	}
  if(SQLITE_OK != sqlite3_bind_int64(statement, 1, whereValue)) {
    fprintf(stderr, "Error binding value in Query (%i): %s \n Query:(%s)\n", rc, sqlite3_errmsg(db), query.c_str());
    //sqlite3_close(db);
    //exit(1);
  }
  if(SQLITE_DONE != sqlite3_step(statement)) {
   fprintf(stderr, "Update statement didn't return DONE (%i): %s\n", rc, sqlite3_errmsg(db));
 }
}

//execute statements
void DB_Manager::executeInsertStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string>> _values){
  openDB();
  for (std::vector<std::string> &a : _values){
      prepInsertStatement(a, getInsertSQL(table, args));
  }
  sqlite3_finalize(statement);
  closeDB();
}

std::vector<std::vector<std::tuple<std::string, std::string>>> DB_Manager::executeSelectStatement(std::string table, std::vector<std::string> args){
  openDB();
  prepSelectStatement(getSelectSQL(table, args));
  std::vector<std::vector<std::tuple<std::string, std::string>>> results_ = fetchSelectResults();
  sqlite3_finalize(statement);
  closeDB();
  return results_;
}

std::vector<std::vector<std::tuple<std::string, std::string>>> DB_Manager::executeSelectStatement(std::string table, int value, std::string whereClause, std::string whereOperator, std::vector<std::string> args){
  openDB();
  std::cout << getSelectSQL(table, args, whereClause, whereOperator) << std::endl;
  prepSelectStatement(value, getSelectSQL(table, args, whereClause, whereOperator));
  std::vector<std::vector<std::tuple<std::string, std::string>>> results_ = fetchSelectResults();
  sqlite3_finalize(statement);
  closeDB();
  return results_;
}

std::vector<std::vector<std::tuple<std::string, std::string>>> DB_Manager::executeSelectStatement(std::string table, std::string value, std::string whereClause, std::string whereOperator, std::vector<std::string> args){
  openDB();
  std::cout << getSelectSQL(table, args, whereClause, whereOperator) << std::endl;
  prepSelectStatement(value, getSelectSQL(table, args, whereClause, whereOperator));
  std::vector<std::vector<std::tuple<std::string, std::string>>> results_ = fetchSelectResults();
  sqlite3_finalize(statement);
  closeDB();
  return results_;
}

void DB_Manager::executeUpdateStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string> > _values, std::string whereClause, std::string whereOperator, std::string whereValue){
  openDB();
  for (std::vector<std::string> &a : _values){
      prepUpdateStatement(a, whereValue, getUpdateSQL(table, args, whereClause, whereOperator));
  }
  sqlite3_finalize(statement);
  closeDB();
}

void DB_Manager::executeUpdateStatement(std::string table, std::vector<std::string> args, std::vector<std::vector<std::string> > _values, std::string whereClause, std::string whereOperator, int whereValue){
  openDB();
  for (std::vector<std::string> &a : _values){
      prepUpdateStatement(a, whereValue, getUpdateSQL(table, args, whereClause, whereOperator));
  }
  sqlite3_finalize(statement);
  closeDB();
}

void DB_Manager::executeDeleteStatement(std::string table, std::string whereClause, std::string whereOperator, std::string whereValue){
  openDB();
  prepDeleteStatement(whereValue, getDeleteSQL(table, whereClause, whereOperator));
  sqlite3_finalize(statement);
  closeDB();
}

void DB_Manager::executeDeleteStatement(std::string table, std::string whereClause, std::string whereOperator, int whereValue){
  openDB();
  prepDeleteStatement(whereValue, getDeleteSQL(table, whereClause, whereOperator));
  sqlite3_finalize(statement);
  closeDB();
}

//opening and closing the database
void DB_Manager::openDB(){
  rc = sqlite3_open(kDb_path, &db);
}

void DB_Manager::closeDB(){
  rc = sqlite3_close(db);
}
