# import socket
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# address = '127.0.0.1'
# port = 8000  # port number is a number, not string
# try:
#     s.connect(('ws://127.0.0.1/ws', port))
#     print("connected")
#     s.send("exit")
#     print("send data")
#     data = s.recv(1024).decode()
#     print (data)
# except Exception as e:
#   print("something's wrong with %s:%d. Exception is %s" % (address, port, e))
# finally:
#     s.close()

# !/usr/bin/python

# !/usr/bin/python
import websocket
import thread
import time


def on_message(ws, message):
    print message


def on_error(ws, error):
    print error


def on_close(ws):
    print "### closed ###"


def on_open(ws):
    def run(*args):
        for i in range(30000):
            time.sleep(1)
            ws.send("Hello %d" % i)
        time.sleep(1)
        ws.close()
        print "thread terminating..."
    thread.start_new_thread(run, ())


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://127.0.0.1:8080/websocket",
                                on_message=on_message, on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open

    ws.run_forever()
