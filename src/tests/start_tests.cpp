#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file


#include "../lib/catch/catch.hpp"
#include "../lib/fakeit/fakeit.hpp"
#include "../header/comm_hub.hpp"
#include "../header/mqtt_controller.hpp"
#include "../header/mqtt_client.hpp"


unsigned int Factorial( unsigned int number ) {
  return number > 1 ? Factorial(number-1)*number : 1;
}

// unit test to check if everything works
TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( Factorial(0) == 1 );
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}

TEST_CASE( "add action to comm hub", "[AddAction]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("testsource", "testdata", "testsource","jojo");
    std::vector< std::shared_ptr<Action>> actions = hub->get_actions("testsource");
    REQUIRE( !actions.empty() );
    delete hub;
}

TEST_CASE( "add action to comm hub and delete", "[AddDELETEAction]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("testsource", "testdata", "testsource","jojo");
    std::vector< std::shared_ptr<Action>> actions = hub->get_actions("testsource");
    REQUIRE( !actions.empty() );
    hub->deleteHash("testsource");
    actions = hub->get_actions("testsource");
    REQUIRE( actions.empty() );
    delete hub;
}

TEST_CASE( "verify register route called", "[Regroute]" ) {
  //use fakeit in ths scope. Action is defined in there :/
  using namespace fakeit;

  Mock<Mqtt_controller> mock;
  Mqtt_controller &i = mock.get();
  When(Method(mock,register_Route)).AlwaysReturn();
  // Production code
  i.register_Route("too/dank", NULL);
  // Verify method mock.foo was invoked.
  Verify(Method(mock,register_Route));
}

TEST_CASE( "get all actions", "[getact]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("testsource", "testdata", "testsource","jojo");
    std::vector< std::shared_ptr<Action>> actions = hub->get_all_actions();
    REQUIRE( !actions.empty() );
    delete hub;
}

TEST_CASE( "register controller", "[cont]" ) {
  Mqtt_client *client = new Mqtt_client();
  Mqtt_controller *api = new Mqtt_api();
  client->register_controller(api);
  REQUIRE( !client->get_controllers().empty() );
  delete client;
}

TEST_CASE( "register NULL controller", "[contNULL]" ) {
  Mqtt_client *client = new Mqtt_client();
  Mqtt_controller *api = NULL;
  client->register_controller(api);
  REQUIRE( client->get_controllers().empty() );
  delete client;
}

//this one fails if delete hub???
TEST_CASE( "add empty source action to comm hub", "[AddNullAction]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("", "testdata", "","jojo");
    std::vector< std::shared_ptr<Action>> actions = hub->get_actions("testsource");
    REQUIRE( actions.empty() );
    delete hub;
}

TEST_CASE( "add empty data action to comm hub", "[AddNulldataAction]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("", "testdata", "source","dest");
    std::vector< std::shared_ptr<Action>> actions = hub->get_actions("testsource");
    REQUIRE( actions.empty() );
    delete hub;
}

TEST_CASE( "add action and check the url", "[checkurlaction]" ) {
    Comm_Hub *hub = new Comm_Hub();
    hub->create_action("testdestdata", "testdata", "source","dest");
    std::vector< std::shared_ptr<Action>> actions = hub->get_actions("testsource");
    for( std::shared_ptr<Action> action : actions)
    {
      REQUIRE( action->source == "source" );
    }
    delete hub;
}

//tests to check execution of actions --------------------------------
TEST_CASE( "execute http action", "[executehttpaction]" ) {
  using namespace fakeit;
  Mock<Http_sender> mock;
  When(Method(mock,send)).AlwaysReturn();
  ISender &i = mock.get();
  Sender_Action *act = new Sender_Action(&i,"jo","jo","jo","jo");
  act->Execute("",false);
  Verify(Method(mock,send));
}

// TEST_CASE( "execute mqtt action", "[executemqttaction]" ) {
//   using namespace fakeit;
//   Mock<Mqtt_client> mock;
//   When(Method(mock,send_message)).AlwaysReturn();
//   When(Method(mock,start)).AlwaysReturn();
// //  When(Method(mock, )).AlwaysReturn();
//   Mqtt_client &i = mock.get();
//   Mqtt_Action *act = new Mqtt_Action(&i,"jo","jo","jo","jo");
//   act->Execute("",false);
//   Verify(Method(mock,send_message));
// }
