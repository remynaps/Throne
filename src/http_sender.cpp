#include "header/htpp_sender.hpp"

using curl::curl_easy;
using curl::curl_easy_exception;
using curl::curlcpp_traceback;

Http_sender::~Http_sender()
{

}

void Http_sender::send_data(const char* url, const char* data)
{
  curl_easy easy;
  // Add some option to the curl_easy object.
  easy.add<CURLOPT_URL>(url);
  easy.add<CURLOPT_FOLLOWLOCATION>(1L);
  try {
    // Execute the request.
    easy.perform();
  } catch (curl_easy_exception error) {
    // If you want to get the entire error stack we can do:
    error.print_traceback();
    // Note that the printing the stack will erase it
  }
}

void Http_sender::send(const char* url)
{
  curl_easy easy;
  // Add some option to the curl_easy object.
  easy.add<CURLOPT_URL>(url);
  easy.add<CURLOPT_FOLLOWLOCATION>(1L);
  try {
    // Execute the request.
    easy.perform();
  } catch (curl_easy_exception error) {
    // If you want to get the entire error stack we can do:
    error.print_traceback();
    // Note that the printing the stack will erase it
  }
}

void Http_sender::send_async(const char* url, const char* data)
{
  std::cout << url << std::endl;
  std::cout << "-------" << std::endl;

  std::thread t1(&Http_sender::send, this, url);
  t1.detach();
}
