#include "header/action.hpp"


Action::Action(std::string source,std::string dest,std::string data,std::string destData)
{
  this->source = source;
  this->dest = dest;
  this->data = data;
  this->destData = destData;
}

ComparisonAction::ComparisonAction(ISender *sender,std::string source,std::string dest,std::string data,std::string dest_data) : Action(source, dest,data,dest_data)
{
  this->source = source;
  this->dest = dest;
  this->sender_ = sender;
}

Mqtt_Action::Mqtt_Action(Mqtt_client *mqtt_,std::string data,std::string destData,std::string source, std::string dest) : Action(source,dest,data,destData)
{
    this->mqtt_ = mqtt_;
}

//check if data is not empty. this makes it possible to directly send data with an action
bool Mqtt_Action::Execute(string data, bool async)
{
  if(this->dest_data == "")
  {
    mqtt_->send_message(data.c_str(), this->dest.c_str());
    return true;
  }
  mqtt_->send_message(this->dest_data.c_str(), this->dest.c_str());
  return true;
}

Sender_Action::Sender_Action(ISender *sender_,std::string data,std::string dest_data,std::string source, std::string dest) : Action(source,dest, data, dest_data)
{
    this->sender_ = sender_;
    this->data = data;
}

bool Sender_Action::Execute(string data, bool async)
{
  if(data == "")
  {
    if(async)
    {
      sender_->send_async(this->dest.c_str());
      return true;
    }
    else{
      sender_->send(this->dest.c_str());
      return true;
    }
  }
  return false;
}

LargerThenAction::LargerThenAction(ISender *sender_,int data, int compare_data,std::string source, std::string dest
  ,std::string sdata,std::string dest_data)
 : ComparisonAction(sender_,source,dest,sdata,dest_data)
{
    this->sender_ = sender_;
    this->data = data;
    this->_compare_data = compare_data;
}

bool LargerThenAction::Execute(int data, bool async)
{
  if(data > _compare_data)
  {
    sender_->send(dest.c_str());
    return true;
  }
  return false;
}

SmallerThenAction::SmallerThenAction(ISender *sender_,int data, int compare_data,std::string source, std::string dest
  ,std::string sdata,std::string dest_data)
 : ComparisonAction(sender_,source,dest,sdata,dest_data)
{
    this->sender_ = sender_;
    this->data = data;
}

bool SmallerThenAction::Execute(int data, bool async)
{
  if(data < _compare_data)
  {
    sender_->send(dest.c_str());
    return true;
  }
  return false;
}
