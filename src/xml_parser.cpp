/*
Hidde Westerhof / xml_parser.cpp / 13-2-2016 / version 0.0.0.1.
Source code for xml_parser.hpp
Reads and writes to system.xml file.
*/

#include "./header/xml_parser.hpp"

std::vector<XML_Data> XML_Parser::getXMLcontent(){
  return this->filecontent_;
}

void XML_Parser::readXML(pugi::xml_node value){
  if(value.next_sibling() != NULL){
    readXML(value.next_sibling());
  }
  if(value.first_child() != NULL){
    readXML(value.first_child());
  }
  if(value.first_attribute() != NULL){
    filecontent_.push_back(XML_Data(value.name(), value.first_attribute().value()));
  }
}

XML_Parser::XML_Parser(const char* filename){
  xmlDocument_.load_file(filename, (pugi::parse_default | pugi::parse_comments) & ~pugi::parse_escapes);
  xmlDocRoot_ = xmlDocument_.child(kXMLRootNode);
}
