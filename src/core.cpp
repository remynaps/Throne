/*
Hidde Westerhof / core.cpp / 13-2-2016 / version 0.0.0.1.
This object is the main body of the project.
Sets up all the different objects.
*/
#include "header/api.hpp"
#include "header/logger.hpp"
#include "lib/mongoose/mongoose/Server.h"
#include "header/comm_hub.hpp"
#include "header/sender_locator.hpp"
#include "header/htpp_sender.hpp"
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <exception>
#include <signal.h>


volatile static bool running = true;

void handle_signal(int sig)
{
    if (running) {
        running = false;
    }
}

int kServerPort = 8080;
Mongoose::Server server(kServerPort);
Comm_Hub *hub;
Api *api_;

bool startApi(){
  try{
      api_ = new Api(hub);
      return true;
  } catch(exception& e){
    LOG("API ERROR", e.what());
    return false;
  }
}

bool startCommHub(){
  try{
      hub = new Comm_Hub();
      return true;
  } catch(exception& e){
    LOG("COMM HUB ERROR", e.what());
    return false;
  }
}

bool startServer(){
  try{
    LOG("EVENT","Starting RESTFUL server..");
    server.setOption("ssl_certificate", "ssl_cert.pem");
    server.registerController(api_);
    server.start();
    LOG("EVENT","REST server started succesfully");
    return true;
  }
  catch(exception &e){
    LOG("SERVER ERROR", e.what());
    server.stop();
    return false;
  }
}

//register your sender types here
bool registerSenders(){
  try{
    LOG("EVENT","Registiring senders...");
    SenderFactory::instance().AddType<Http_sender>("http");
    return true;
  }
  catch(exception &e){
    LOG("REGISTER ERROR", e.what());
    return false;
  }
}

int main(int argc, char** argv){

      //handle the exit signal
      signal(SIGINT, handle_signal);

      std::cout << " _____ _____ _____ _____ _____ _____ " << std::endl;
      std::cout << "|_   _|  |  | __  |     |   | |   __|" << std::endl;
      std::cout << "  | | |     |    -|  |  | | | |   __|" << std::endl;
      std::cout << "  |_| |__|__|__|__|_____|_|___|_____|" << std::endl;

      //initialize the communications hub
      //system("launchctl load /usr/local/Cellar/mosquitto/1.4.8/homebrew.mxcl.mosquitto.plist");
      if(startCommHub()){
        if(startApi()){
          if(startServer()){
            if(registerSenders())
            {
              while (running) {
                  sleep(10);
                  //update the hub componenets if necesary
                  hub->update();
              }
              LOG("EVENT","Shut down command given.");
              LOG("EVENT","Cleaning up command.");
              delete hub;
              delete api_;
              LOG("EVENT","Stopping Server");
              server.stop();
            }
          }
        }
      }
      return 0;
  }
