import xml.etree.ElementTree as ET
import sys
import hashlib, uuid
import os
from random import randint
from passlib.hash import pbkdf2_sha256

strWelcome = "Welcome to the THRONE setup script.\r\nIn this script we'll ask you a bunch of questions, but we don't need them answered immediatley.\r\nThey'll help figure out some important aspects of your network."
strCore = "Is this component the CORE of your network?"
strAsk = "Y/N?"
strEnterPw = "Please enter your CORE password. This password will be used to link components in your network with the Core."
strGenPw = "Please write down and/or remember the following password:"

isCore = ""
strPassword = ""

system = ET.Element("system")
component = ET.SubElement(system, "component")
xmlTree = ET.ElementTree(system)

article = ["The","A", "An","That","This","Our",""]
nouns = ["cat", "goat", "remy", "pieter", "nigel", "Jos", "dog", "shepard", "Lord", "lovers", "duke", "EU", "Thomas", "Marloes", "Lieneke", "cow","children", "raccoon", "zombies"]
verbs = ["hates","loves","stabbed","poisoned","killed","cried","milked","cheered","programmed","seperated","dictated","kissed","hugged","fucked","cowsaid","said","got","bitten","brained","walked"]
adjectives = ["women","men","feminists","russia","shepard","lovely","powerarmor","settlement","insitute","magic","you","me","god","acow","acrow","thecat","zombies","japan","cherry","brother","sister"]
special = ["@","-","_","!","+","*"]

def addElement(string_Elementname, userInput, parent=component):
    ET.SubElement(parent, string_Elementname, value=userInput)

def saveSystemFile():
    xmlTree = ET.ElementTree(system)
    xmlTree.write("system.xml")

def generateNumber(higest):
    return randint(0,higest)

def generateSentence():
    sentence = article[generateNumber(len(article)-1)]
    sentence += nouns[generateNumber(len(nouns)-1)].title()
    sentence += verbs[generateNumber(len(verbs)-1)].title()
    sentence += adjectives[generateNumber(len(adjectives)-1)].title()
    coinToss = randint(0,3)
    specialchar = special[generateNumber(len(special)-1)]
    if coinToss > 1:
        sentence += specialchar
    else:
        sentence = specialchar + sentence

    return sentence

def generatePassword():
    return generateSentence()

def hashPassword(password):
    return pbkdf2_sha256.encrypt(password.encode('utf-8'), rounds=200000, salt_size=16)

def askQuestion():
    while True:
        if input().upper() == 'Y':
            return "1"
        elif input().upper() == 'N':
            return "0"
        else:
            print("Please use Y or N as input.")

def firstTime():
    if os.path.isfile(".\system.xml"):
        return False
    else:
        return True

def say(text):
    print(text)
    print()

def main(argv):
    say(strWelcome)

    if not firstTime():
        say("You have ran setup on this component before. Would you like to reset and rerun setup?")
        say(strAsk)
        if askQuestion() == 'N':
            return
        else:
            say("Deleting previous system files and settings")
            os.remove(".\system.xml")

    say(strCore)
    say(strAsk)

    isCore = askQuestion()
    addElement("Systype", isCore, system)
    if isCore == "1":
        say(strGenPw)
        pwd = generatePassword()
        say(pwd)
    else:
        say(strEnterPw)
        pwd = input()

    addElement("password", hashPassword(pwd))


    saveSystemFile()

if __name__ == '__main__':
    main(sys.argv)
