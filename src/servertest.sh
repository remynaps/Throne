#!/bin/sh

PROG=$1
PORT=${2:-8080}  # If second param is given, this is load balancer port
URL=127.0.0.1:$PORT/device/status
URL2=127.0.0.1:$PORT/device/jsonstatus
URL3=127.0.0.1:$PORT/action


cleanup() {
  kill -9 $PID >/dev/null 2>&1
}

#set -x
trap cleanup EXIT

cleanup

PID=$!

#sleep 1
curl -s -X GET -d 'device=' $URL
# Fetch existing key
# RESULT=$(curl -s $URL/bar/baz)
# test "$RESULT" = "success" || exit 1

curl -s -X GET -d 'device=' $URL2


curl --data "param1=value1&param2=value2" $URL3

exit 0
