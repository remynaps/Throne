/*
Hidde Westerhof / api.cpp / 13-2-2016 / version 0.0.0.1.
Source code for api.hpp
Interface for agent_api and client_api. They will implement these functions.
*/

#include "header/api.hpp"
#include "header/comm_hub.hpp"
#include "lib/jsoncpp/json/json.h"
#include "header/logger.hpp"

Api::Api(Comm_Hub *_hub)
{
  hub = _hub;
  init(); //register routes for the controller
}

Api::~Api()
{

}
//add all the api handler methods here
void Api::init()
{
  addRoute("GET", "/hello", Api, http_GET);
  addRoute("GET", "/device/status", Api, GET_Device_Status);
  addRoute("POST", "/action", Api, POST_Action);
  addRoute("POST", "/topic", Api, POST_Topic);
  addRoute("GET", "/actions", Api, Process_Action);
  addRoute("POST", "/action/direct", Api, Call_action);
  addRoute("POST", "/actions/delete", Api, Call_Delete);
  addRouteResponse("GET", "/actions", Api, Get_Actions, JsonResponse);
  addRouteResponse("GET", "/topics", Api, Get_Topics, JsonResponse);
  addRouteResponse("GET", "/subscribtions", Api, Get_Subscribers, JsonResponse);
  addRouteResponse("GET", "/device/jsonstatus", Api, GET_JSON_Device_Status, JsonResponse);
}

void Api::start()
{ //todo seperate server and controllers

}
  // request to /hello?name=123
  // if value is name print hello <value of name> else Hello + whats your name?
void Api::http_GET(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  response << "Hello " << htmlEntities(request.get("name", "... what's your name ?")) << endl;
  hub->send_mqtt("1", "dank/memes");
}

void Api::GET_Device_Status(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  string getValue = htmlEntities(request.get("device", ""));
  int status;
  if(getValue != "")
  {
    status = std::stoi(getValue);
    response << "Status : " << status;
  }
    else
  {
      response << "Status was NULL";
  }
}

void Api::GET_JSON_Device_Status(Mongoose::Request &request, Mongoose::JsonResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  int i;

  for (i=0; i<2; i++) {
      response["users"][i]["Name"] = "Bob";
  }

  response["timestamp"] = (int)time(NULL);
}

void Api::POST_Action(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  Json::Value source;
  Json::Value destination;
  Json::Value type;
  Json::Value sourcedata;
  Json::Value destinationdata;

  if(not reader.parse(request.get("data"),root,false))
  {
    cout<<"Failed to parse JSON"<<endl <<reader.getFormatedErrorMessages()<<endl;
  }
  if(Parse_Action_Json(root, source,destination,type,sourcedata, destinationdata))
  {
    string charsource = "";
    string chardest = "";
    cout << destination.asString() << endl;
    if(destination.asString() == source.asString() || destination.asString().compare(0,7,"throne/") == 0)
    {
      LOG("DEST ERROR", "THRONE cannot send messages to itself. try removing the throne/ prefix from your dest");
      return;
    }
    hub->create_action(sourcedata.asString(), destinationdata.asString(), source.asString(),destination.asString(),type.asString());
  }
}
// for (int i = 0; i < source.asString().length(); ++i){
//    charsource[i] = tolower(source.asString()[i]);
// }
// for (int i = 0; i < destination.asString().length(); ++i){
//    chardest[i] = tolower(destination.asString()[i]);
// }

//try to parse the json file
bool Api::Parse_Action_Json(Json::Value &root, Json::Value &source,Json::Value &destination,
  Json::Value &type,Json::Value &sourcedata,Json::Value &destinationdata)
{
  try
  {
    source = root["source"];
    destination = root["destination"];
    type = root["type"];
    sourcedata = root["data"];
    destinationdata = root["destdata"];

    return true;
  }
  catch(exception& e)
  {
      LOG("ParseError", "One or more json values do not exist")
  }
  return false;
}

void Api::Process_Action(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  hub->process_action(request.getUrl());
}

void Api::Get_Actions(Mongoose::Request &request, Mongoose::JsonResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  string getValue = htmlEntities(request.get("device", ""));

  std::vector<string> results = hub->get_all_action_sources();
  for (int i = 0; i < results.size(); i++) {
      response[i]["topicName"] =  results[i];
  }
}

void Api::Get_Topics(Mongoose::Request &request, Mongoose::JsonResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  string getValue = htmlEntities(request.get("device", ""));

  std::vector<std::vector<std::tuple<std::string, std::string>>> results;
  results = hub->db_layer->Get_All("Topic");

  for(int i = 0; i < results.size(); i++){
    LOG("INTHRONE", std::get<1>(results[i][1]));
    response[i]["id"]   =  std::get<1>(results[i][0]);
    response[i]["topicName"] =  std::get<1>(results[i][1]);
    LOG("JSON", response[i]["topicName"].asString());
  }
}

void Api::Get_Subscribers(Mongoose::Request &request, Mongoose::JsonResponse &response)
{
  response.setHeader("Access-Control-Allow-Origin", "*");
  string getValue = htmlEntities(request.get("device", ""));

  std::vector<std::vector<std::tuple<std::string, std::string>>> results;
  results = hub->db_layer->Get_All("Subscribers");
  for(int i = 0; i < results.size(); i++){
      response[i]["id"]   =  std::get<1>(results[i][0]);
      response[i]["topicName"] =  std::get<1>(results[i][1]);
  }
}

void Api::POST_Topic(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  Json::Value topic;

  if(not reader.parse(request.get("data"),root,false))
  {
    cout<<"Failed to parse JSON"<<endl <<reader.getFormatedErrorMessages()<<endl;
    return;
  }
  topic = root["topic"];
  cout<<"topic is" << topic.asString() <<endl;

  hub->db_layer->create_topic(topic.asString());
}

void Api::Call_Delete(Mongoose::Request &request, Mongoose::StreamResponse &response){
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  Json::Value topic;

  if(not reader.parse(request.get("data"),root,false))
  {
    cout<<"Failed to parse JSON"<<endl <<reader.getFormatedErrorMessages()<<endl;
    return;
  }
  hub->deleteHash(root["topic"].asString());
}

void Api::Call_action(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  Json::Value topic;
  Json::Value data;

  if(not reader.parse(request.get("data"),root,false))
  {
    cout<<"Failed to parse JSON"<<endl <<reader.getFormatedErrorMessages()<<endl;
    return;
  }
  topic = root["destination"];
  data = root["data"];
  if(data != "")
  {
    hub->process_action(topic.asString(), data.asString());
    return;
  }
  hub->process_action(topic.asString());
}
