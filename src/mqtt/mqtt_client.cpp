#include "../header/mqtt_client.hpp"
#include <iostream>
#include <string.h>
#include "../header/structs.hpp"

Mqtt_client::Mqtt_client(const char * _id,const char * _topic, const char * _host, int _port, bool async) : mosquittopp(_id)
 {
     this->keepalive = 60;                    // Basic configuration setup for myMosq class
     this->id = _id;
     this->port = _port;
     this->host = _host;
     this->topic = _topic;
     start(async);
 };

 void Mqtt_client::start(bool async)
 {
   if(async)
   {
     mosqpp::lib_init();                      // Mandetory initialization for mosquitto library
     connect_async(host, port, keepalive);   // non blocking connection to broker request

     loop_start();                           // Start thread managing connection / publish / subscribe
   }
   else
   {
     mosqpp::lib_init();                      // Mandetory initialization for mosquitto library
     connect(host, port, keepalive);   // non blocking connection to broker request
   }
 }

 Mqtt_client::Mqtt_client()  {  };

 Mqtt_client::~Mqtt_client() {
   disconnect();              //disconnect from the broker
   loop_stop(true);           // Kill the thread
   mosqpp::lib_cleanup();     // Mosquitto library cleanup
   std::cout << "mqtt stopped" << std::endl;
 }

// *register a controller with the client.
// *every controller consists of a number of routes.
// *this helps to increase seperation of concerns for api requests
 void Mqtt_client::register_controller(Mqtt_controller *cont)
 {
   if(cont != NULL)
   {
     controllers.push_back(cont);
   }
 }

 //return all controllers in this client
 std::vector<Mqtt_controller*> Mqtt_client::get_controllers()
 {
    return controllers;
 }

/*
cast the recieved message to an mqtt message.
this is done to increase compatibility and add the ability to pass a message
through the application without having to import a library to support the
mosquitto message
*/
 void Mqtt_client::on_message(const struct mosquitto_message *message)
 {
     struct mqtt_message msg;
     msg.mid          = message->mid;
     msg.topic        = message->topic;
     msg.payload      = (char*)message->payload;
     msg.payloadlen   = message->payloadlen;
     msg.qos          = message->qos;
     msg.retain       = message->retain;

     //cast the topic to all lower case chars.
     //to remove dependance on uppercase
     for (int i = 0; i < strlen(msg.topic); ++i){
        msg.topic[i] = tolower(msg.topic[i]);
     }

     //search the controllers to process the request
     for(Mqtt_controller *cont : controllers) {
        cont->process(&msg);
     }
 }

 void Mqtt_client::on_subcribe(int mid, int qos_count, const int *granted_qos)
 {
	  std::cout << "sucsessful subd" << std::endl;
 }

 // Send message - depending on QoS, mosquitto lib managed re-submission this the thread
 //
 // * NULL : Message Id (int *) this allow to latter get status of each message
 // * topic : topic to be used
 // * lenght of the message
 // * message
 // * qos (0,1,2)
 // * retain (boolean) - indicates if message is retained on broker or not
 // Should return MOSQ_ERR_SUCCESS
 bool Mqtt_client::send_message(const  char * _message,const  char * _topic)
 {
     int ret = publish(NULL,_topic,strlen(_message),_message,1,false);
     if(ret != MOSQ_ERR_SUCCESS)
     {
       std::cout << ret << std::endl;
       std::cout << "error" << std::endl;
     }
     return ( ret == MOSQ_ERR_SUCCESS );
 }

 void Mqtt_client::on_disconnect(int rc) {
     std::cout << ">> myMosq - disconnection(" << rc << ")" << std::endl;
 }

// *callback used to handle the connection event.
// *rc: int used to determine the state of the connection. should be 0
// *if rc is not zero there is a problem with the broker connection
// it subscribes on all topics starting with throne/ to monitor traffic when
// the connection is succesful
 void Mqtt_client::on_connect(int rc)
 {
     if ( rc == 0 ) {
         std::cout << ">> myMosq - connected with server" << std::endl;
         subscribe(NULL, "throne/#");
     } else {
         std::cout << ">> myMosq - Impossible to connect with server(" << rc << ")" << std::endl;
     }
 }

 void Mqtt_client::on_publish(int mid)
 {
     std::cout << ">> MQTT - Message (" << mid << ") succeed to be published " << std::endl;
 }
