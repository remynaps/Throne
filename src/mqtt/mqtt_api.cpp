#include "../header/mqtt_api.hpp"
#include "../header/comm_hub.hpp"
#include <iostream>
#include <string.h>

Mqtt_api::Mqtt_api(Comm_Hub *_hub) : Mqtt_controller()
{
    Mqtt_controller::add_Route("throne/dank/memes", Mqtt_api, handle_light);
    Mqtt_controller::add_Route("throne/post/sensor", Mqtt_api, handle_sensor);
    Mqtt_controller::add_Route("throne/action/(.*)", Mqtt_api, handle_treshold);
    hub = _hub;
}

Mqtt_api::Mqtt_api() : Mqtt_controller()
{
}

void Mqtt_api::handle_light(const struct mqtt_message* msg)
{
    hub->process_alert(msg->topic);
}

//handle data coming from a sensor
void Mqtt_api::handle_sensor(const struct mqtt_message* msg)
{
    hub->process_sensor(msg->topic);
}

void Mqtt_api::handle_treshold(const struct mqtt_message* msg)
{
    std::string *sp = static_cast<std::string*>(msg->payload);
    hub->process_action(msg->topic, (char*)sp);
}
