#include "../header/mqtt_controller.hpp"
#include <iostream>
#include <string.h>
#include <regex>
#include <map>

Mqtt_controller::Mqtt_controller()
{

}

void Mqtt_controller::register_Route(std::string route,Mqtt_handler_base *handler)
{
    //add a new record to the routes dict including a pointer to a method
    routes[route] = handler;
}

//regex match the route with the given topic. if it is a match the
//given method will process the request
void Mqtt_controller::process(const struct mqtt_message* msg)
{
  map<string, Mqtt_handler_base *>::iterator it;
  for (it=routes.begin(); it!=routes.end(); it++) {
      regex r(it->first);
      if (std::regex_match(msg->topic,r)){
        it->second->process(msg);
        break;
      }
  }
  // if(routes[msg->topic] != NULL)
  // {
  //   routes[msg->topic]->process(msg);
  //   //return (this->*fp)("jo");
}
