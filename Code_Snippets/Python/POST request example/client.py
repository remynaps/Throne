from urllib import request, parse

class WebCommunicator:

    url_endPoint = ""
    data = ""
    request = ""

    def __init__(self):
        print("Created")

    def setUpURL(self, User_data, str_Url = "http://localhost/DEVELOPMENT/Dev-THRONE/file.php"):
        self.data = parse.urlencode({'data': User_data})
        self.request = request.Request(str_Url)

    def sendData(self):
        self.data = self.data.encode('utf-8')
        self.request.add_header("Content-Type","application/x-www-form-urlencoded;charset=utf-8")
        if self.data is not "":
            #logsite = "http://localhost/DEVELOPMENT/Dev-THRONE/log.php"
            try:
                f = request.urlopen(self.request, self.data)
            except:
                print("Cannot connect to the website at this point. Is the server running? Otherwise, check if your URL is correct.")
            #You can use f for further data checking and such.
        else:
            return
