from multiprocessing import Process, Queue
from ipcCode import P

def main():
    p = P()
    p.start()

    x = input()
    while x is not "q":
        x = input()

    p.join()

if __name__ == '__main__':
    main()
