#include <iostream>
#include "./db_manager.hpp"


int main(){
    DB_Manager::instance().executeInsertStatement("table2", {"name","lname"}, {{"Elon","Musk"},{"Dick","Bruin"}});

    //dbm.executeSelectStatement("table2", {"name"});
    //dbm.executeSelectStatement("table2", 2, "id", "=");
    DB_Manager::instance().executeUpdateStatement("table2", {"name","lname"}, {{"Sibbele","Oosterhaven"}}, "id", "=", 1);

    //dbm.executeSelectStatement("table2", {"name"});

    DB_Manager::instance().executeDeleteStatement("table2", "id", "=", 1);

    DB_Manager::instance().executeSelectStatement("table2");

    printf("Done\n");

}
